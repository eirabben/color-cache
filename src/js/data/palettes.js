const palettes = [
  {
    highlightColor: '#4483d9',
    contrastColor: '#4f4f4f',
    backgroundColor: '#f7f7f7',
    source: 'http://www.steveschoger.com'
  },
  {
    highlightColor: '#fffae1',
    contrastColor: '#323232',
    backgroundColor: '#a03741',
    source: 'http://htmlarrows.com/symbols/'
  },
  {
    highlightColor: '#ffffff',
    contrastColor: '#004e74',
    backgroundColor: '#0088cb'
  },
  {
    highlightColor: '#07b57a',
    contrastColor: '#666666',
    backgroundColor: '#f8f6f2',
    source: 'https://typekit.com/marketplace'
  },
  {
    highlightColor: '#ffffff',
    contrastColor: '#3a3a3a',
    backgroundColor: '#ee4938'
  },
  {
    highlightColor: '#17e7a4',
    contrastColor: '#f5f7fa',
    backgroundColor: '#9883e5',
    source: 'https://cactomain.co/'
  },
  {
    highlightColor: '#5bd9b3',
    contrastColor: '#969696',
    backgroundColor: '#3e3c36',
    source: 'https://www.rodesk.com/'
  }
]

export default palettes
