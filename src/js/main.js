
require('normalize.css')
require('../sass/main.scss')

import Vue from 'vue'
import App from './App.vue'

const app = new Vue({
  el: '#app',
  render: h => h(App)
})
